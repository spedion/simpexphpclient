# README #

This SPEDION Simpex Webservice demo project was created using NetBeans IDE, which creates the folder
**nbproject**. If do not use this IDE, you can ignore or delete this directory.

## Requirements ##
The php modules/extensions **soap**, **openssl** and **libxml** need to activated.

You should create **src/example.ini** like this:
    user = yourUser
    password = yourPassword

Please enable WSDL cache, otherwise the WSDL will be downloaded before each 
MessageService call.

Check your php.ini file and take a look at 
[soap.configuration.php](http://php.net/manual/en/soap.configuration.php)

### Timezone is UTC ###
As of Simpex version 3.1 all DateTime object contain UTC timestamps.

If possible set up UTC time zone in php.ini or use

    date_default_timezone_set('UTC');

otherwise you have to code like this:

    $messageTimeUtc = new DateTime();
    $messageTimeUtc->setTimezone(new DateTimeZone('UTC'));

The format of the DateTime objects should be formatted with this method:

    $messageTimeUtc->format('Y-m-d\TH:i:s\Z');

## Start ##
The ExampleProgram.php send a **text message** and a minimal **tour** to the vehicle **13579**.

    php ExampleProgram.php