<?php

use Spedion\ServiceClientBundle\ServiceClients\MessageService\Credentials;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\AdditionalValueItem;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\AdditionalValues;
use Spedion\SimpexFactory;
use Spedion\SimpexForm;
use Spedion\MessageServiceFacade;
use Spedion\Simpex;

require 'init.php';

/*
 * Example tour with four places and three orders.
 * Place A
 *  - load order 1
 *  - load order 2
 * Place B
 *  - load order 3
 * Place C
 *  - unload order 2
 *  - unload order 3
 * Place D
 *  - unload order 1
 */

/*
 * initializing
 */
$ini = parse_ini_file('example.ini');
$credentials = new Credentials($ini['user'], $ini['password']);
$msgSvc = new MessageServiceFacade($credentials);

/*
 * Create tour example
 */
$date1 = new DateTime();
$date2 = $date1;
$date2->add(new DateInterval('P2D'));
$date3 = $date1;
$date3->add(new DateInterval('PT45M'));
$tourMessage = SimpexFactory::createMessage(
        SimpexForm::NewTour,
        '13579',
        $date1
);

/*
 * The three load- and three unload orders
 */
$spOrder1Load = SimpexFactory::createOrder('1');
$spOrder1Load->Amount = 1;
$spOrder1Load->ProductName = "1";
$spOrder1Load->Type = 'Load';
$spOrder1Load->AdditionalData = new AdditionalValues();
$spOrder1Load->AdditionalData->Items = array();
$item1 = new AdditionalValueItem();
$item1->Key = "Key1";
$item1->Value = "Value1";
$spOrder1Load->AdditionalData->Items[] = $item1;
$item2 = new AdditionalValueItem();
$item2->Key = "Key2";
$item2->Value = "Value2";
$spOrder1Load->AdditionalData->Items[] = $item2;


$spOrder1Unload = SimpexFactory::createOrder('1');
$spOrder1Unload->Amount = 1;
$spOrder1Unload->ProductName = "1";
$spOrder1Unload->Type = 'Unload';

$spOrder2Load = SimpexFactory::createOrder('2');
$spOrder2Load->Amount = 2;
$spOrder2Load->ProductName = "2";
$spOrder2Load->Type = 'Load';
$spOrder2Unload = SimpexFactory::createOrder('2');
$spOrder2Unload->Amount = 2;
$spOrder2Unload->ProductName = "2";
$spOrder2Unload->Type = 'Unload';

$spOrder3Load = SimpexFactory::createOrder('3');
$spOrder3Load->Amount = 3;
$spOrder3Load->ProductName = "3";
$spOrder3Load->Type = 'Load';
$spOrder3Unload = SimpexFactory::createOrder('3');
$spOrder3Unload->Amount = 3;
$spOrder3Unload->ProductName = "3";
$spOrder3Unload->Type = 'Unload';

/*
 * initialize tour 
 */
$spTour = SimpexFactory::createTour('13579', time(), $date1);
$spTour->DriverPin = '1234';
$spTour->CommentFromDispo = 'Test tour';
$spTour->TourBeginUtc = Simpex::dateTimeFormat($date2);
$spTour->TourEndUtc = Simpex::dateTimeFormat($date3);

/*
 * first place A
 */
$spPlaceA = SimpexFactory::createPlace('1');
$spPlaceA->PlaceName = 'A';
$spPlaceA->Address = 'A Str.';
$spPlaceA->Housenr = '7';
$spPlaceA->Nation = 'DE';
$spPlaceA->Zip = '12345';
$spPlaceA->Location = 'Ahbach';
$spPlaceA->PlanBeginUtc = Simpex::dateTimeFormat($date2);
$spPlaceA->PlanEndUtc = Simpex::dateTimeFormat($date3);
$spPlaceA->CommentFromDispo = 'Test A';
$spPlaceA->DisplayAtPos = 1;
// attach orders 1 + 2 to place
$spPlaceA->Orders = array($spOrder1Load, $spOrder2Load);

/*
 * second place B
 */
$spPlaceB = SimpexFactory::createPlace('2');
$spPlaceB->PlaceName = 'B';
$spPlaceB->Address = 'B Str.';
$spPlaceB->Housenr = '7';
$spPlaceB->Nation = 'DE';
$spPlaceB->Zip = '23456';
$spPlaceB->Location = 'Behbach';
$spPlaceB->PlanBeginUtc = Simpex::dateTimeFormat($date2);
$spPlaceB->PlanEndUtc = Simpex::dateTimeFormat($date3);
$spPlaceB->CommentFromDispo = 'Test B';
$spPlaceB->DisplayAtPos = 2;
// attach order 3 to place
$spPlaceB->Orders = array($spOrder3Load);

/*
 * third place C
 */
$spPlaceC = SimpexFactory::createPlace('3');
$spPlaceC->PlaceName = 'C';
$spPlaceC->Address = 'C Str.';
$spPlaceC->Housenr = '7';
$spPlaceC->Nation = 'DE';
$spPlaceC->Zip = '34567';
$spPlaceC->Location = 'Cehbach';
$spPlaceC->PlanBeginUtc = Simpex::dateTimeFormat($date2);
$spPlaceC->PlanEndUtc = Simpex::dateTimeFormat($date3);
$spPlaceC->CommentFromDispo = 'Test C';
$spPlaceC->DisplayAtPos = 3;
// attach order 2 and 3 to place
$spPlaceC->Orders = array($spOrder2Unload, $spOrder3Unload);

/*
 * forth place D
 */
$spPlaceD = SimpexFactory::createPlace('4');
$spPlaceD->PlaceName = 'D';
$spPlaceD->Address = 'D Str.';
$spPlaceD->Housenr = '4';
$spPlaceD->Nation = 'DE';
$spPlaceD->Zip = '45678';
$spPlaceD->Location = 'Dehbach';
$spPlaceD->PlanBeginUtc = Simpex::dateTimeFormat($date2);
$spPlaceD->PlanEndUtc = Simpex::dateTimeFormat($date3);
$spPlaceD->CommentFromDispo = 'Test D';
$spPlaceD->DisplayAtPos = 4;
// attach order 1 to place
$spPlaceD->Orders = array($spOrder1Unload);

/*
 * calling MessageService.AddMessage()
 */
echo PHP_EOL . 'Calling AddMessage() - Tour' . PHP_EOL;
// attach places to tour
$spTour->Places = array($spPlaceA, $spPlaceB, $spPlaceC, $spPlaceD);
// attach tour to message
$tourMessage->Tour = $spTour;
$result = $msgSvc->AddMessage($tourMessage);
print_r($result);
$xml = $msgSvc->GetLastRequest();
print_r($xml);