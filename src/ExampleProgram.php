<?php

use Spedion\ServiceClientBundle\ServiceClients\MessageService\Credentials;
use Spedion\SimpexFactory;
use Spedion\SimpexForm;
use Spedion\MessageServiceFacade;
use Spedion\Simpex;

require 'init.php';

/*
 * initializing
 */
$ini = parse_ini_file('example.ini');
$credentials = new Credentials($ini['user'], $ini['password']);
$msgSvc = new MessageServiceFacade($credentials);

/*
 * GetVersion()
 */
echo 'Calling GetVersion() ' . PHP_EOL;
$version = $msgSvc->GetVersion();
echo 'SimpexWs version: ' . $version . PHP_EOL;

/*
 * GetUnreadMessage()
 */
echo PHP_EOL . 'Calling GetUnreadMessage()' . PHP_EOL;
$messageResult = $msgSvc->GetUnreadMessage();
$messageIdList = array();
if ($messageResult != null && isset($messageResult->result)) {
    print_r($messageResult->result);
    if ($messageResult->result->Code == 'OkCode') {
        foreach ($messageResult->messages as $message) {
            /* @var $message Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Message */
            echo $message->MessageID . ': vehicle ' . $message->Vehicle 
                . ' Form ' . $message->Form . ' (' . $message->FormName . ')'
                . PHP_EOL;
            $messageIdList[] = $message->MessageID;
        }
    }
} else {
    echo "Fetching messages failed";
}

/*
 * SetListOfMessageAsRead()
 */
if (count($messageIdList) > 0) {
    echo PHP_EOL . 'Calling SetListOfMessageAsRead()' . PHP_EOL;
    $setListAsReadResult = $msgSvc->SetListOfMessageAsRead($messageIdList);
    print_r($setListAsReadResult);
}

/*
 * Send text message to vehicle
 */
echo PHP_EOL . 'Calling AddMessage() - Textmessage' . PHP_EOL;
$textMessage = SimpexFactory::createMessage(
        SimpexForm::TextMessageToVehicle /*1501*/,
        "13579" /*vehicle*/,
        new DateTime() /*messageTimeUtc*/
);
$textMessage->Text = "Test Message from php client.";
$textMessage->Reference = "MyResponseIdentifier";
$resultText = $msgSvc->AddMessage($textMessage);
print_r($resultText);

/*
 * Create tour example
 */
echo PHP_EOL . 'Calling AddMessage() - Tour' . PHP_EOL;
$date1 = new DateTime();
$date2 = $date1;
$date2->add(new DateInterval('P2D'));
$date3 = $date1;
$date3->add(new DateInterval('PT45M'));
$tourMessage = SimpexFactory::createMessage(
        SimpexForm::NewTour,
        '13579',
        $date1
);

$spTour = SimpexFactory::createTour('13579', time(), $date1);
$spTour->DriverPin = '1234';
$spTour->CommentFromDispo = 'Test tour';
$spTour->TourBeginUtc = Simpex::dateTimeFormat($date2);
$spTour->TourEndUtc = Simpex::dateTimeFormat($date3);

$spPlace = SimpexFactory::createPlace('1');
$spPlace->PlaceName = 'Krombach';
$spPlace->Address = 'Industriestr.';
$spPlace->Housenr = '7';
$spPlace->Nation = 'DE';
$spPlace->Zip = '63829';
$spPlace->Location = 'Krombach';
$spPlace->PlanBeginUtc = Simpex::dateTimeFormat($date2);
$spPlace->PlanEndUtc = Simpex::dateTimeFormat($date3);
$spPlace->CommentFromDispo = 'Teststadt';
$spPlace->DisplayAtPos = 1;

$spOrder = SimpexFactory::createOrder('1234');
$spOrder->Amount = 2;
$spOrder->DisplayAtPos = 1;
$spOrder->ProductName = "NewOrder";
$spOrder->Type = 'Load';

$spPlace->Orders = array($spOrder);
$spTour->Places = array($spPlace);
$tourMessage->Tour = $spTour;

$result = $msgSvc->AddMessage($tourMessage);
print_r($result);

/*
 * UpdateTour()
 */
echo PHP_EOL . 'Calling UpdateTour()' . PHP_EOL;
$spTour->Places[0]->CommentFromDispo = 'Test tour update :-)';
$upResult = $msgSvc->UpdateTour($spTour);
print_r($upResult);

/*
 * GetTourByTournr()
 */
echo PHP_EOL . 'Calling GetTourByTournr()' . PHP_EOL;
$tour = $msgSvc->GetTourByTourNr($spTour->Tournr);
$tour->Places = null; // print less debug output :-)
print_r($tour);

/*
 * DeleteTourByTournr()
 */
echo PHP_EOL . 'Calling DeleteTourByTournr()' . PHP_EOL;
$delResult = $msgSvc->DeleteTourByTournr($spTour->Tournr);
print_r($delResult);
