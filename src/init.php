<?php
if (!file_exists('example.ini')) {
    throw new Exception(
        'Please create the file example.ini in this directory, '
        . 'with user = YOUR_USERNAME [newline] password = YOUR_PASSWORD'
    );
}
date_default_timezone_set('UTC');
ini_set('display_errors', 'on');
error_reporting(E_ALL);
$vendordir = __DIR__;
$spediondir = $vendordir . DIRECTORY_SEPARATOR . 'spedion';
$messageServiceDir = $vendordir . implode(DIRECTORY_SEPARATOR, array('', 'spedion', 'MessageService', ''));
include $messageServiceDir . 'WSLogger.php';
include $messageServiceDir . 'Generated/MessageServiceBase.php';
include $messageServiceDir . 'MessageService.php';
include $messageServiceDir . 'Credentials.php';
include $spediondir . DIRECTORY_SEPARATOR . 'SimpexFactory.php';
include $spediondir . DIRECTORY_SEPARATOR . 'SimpexForm.php';
include $spediondir . DIRECTORY_SEPARATOR . 'Simpex.php';
include $spediondir . DIRECTORY_SEPARATOR . 'MessageServiceFacade.php';
include $spediondir . DIRECTORY_SEPARATOR . 'Facade' . DIRECTORY_SEPARATOR . 'GetUnreadMessageResponse.php';