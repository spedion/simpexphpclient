<?php

namespace Spedion\Facade;

/**
 * Description of GetUnreadMessageResponse
 *
 * @author jbayer
 */
class GetUnreadMessageResponse
{
    /**
     * @var \Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Message[]
     */
    public $messages = array();
    
    /**
     *
     * @var \Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Result
     */
    public $result;
}
