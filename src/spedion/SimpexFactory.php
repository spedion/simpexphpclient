<?php
namespace Spedion;

use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Message;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Tour;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Place;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Order;

/**
 * Description of SimpexFactory
 *
 * @author jbayer
 */
class SimpexFactory 
{
    /**
     * @return Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Message Message
     */
    public static function createMessage($form, $vehicle, \DateTime $messageTimeUtc)
    {
        $message = new Message();
        
        // fill needed default values
        $message->MessageID = 0;
        $message->VehicleId = 0;
        $message->Latitude = 0;
        $message->Longitude = 0;
        $message->Speed = 0;
        $message->FmsValid = 0;
        $message->Odometer = 0;
        
        // set important properties
        $message->Form = $form;
        $message->MessageTimeUtc = Simpex::dateTimeFormat($messageTimeUtc);
        $message->Vehicle = $vehicle;
        
        return $message;
    }
    
    /**
     * @return Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Tour Tour
     */
    public static function createTour($vehicle, $tournr, \DateTime $tourDateUtc)
    {
        $tour = new Tour();
        // fill needed default values
        $tour->State = 10;
        $tour->DisplayAtPos = 0;
        
        // set important properties
        $tour->Tournr = $tournr;
        $tour->VehicleName = $vehicle;
        $tour->TourDateUtc = Simpex::dateTimeFormat($tourDateUtc);
        return $tour;
    }
    
    /**
     * @return Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Place Place
     */
    public static function createPlace($placeNr)
    {
        $place = new Place();
        // fill needed default values
        $place->State = 10;
        
        // set important properties
        $place->PlaceNr = $placeNr;
        
        return $place;
    }
    
    /**
     * @return Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Order Order
     */
    public static function createOrder($orderNr)
    {
        $order = new Order();
        // fill needed default values
        $order->State = 10;
        
        // set important properties
        $order->OrderNr = $orderNr;
        
        return $order;
    }
}
