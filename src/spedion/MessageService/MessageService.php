<?php

namespace Spedion\ServiceClientBundle\ServiceClients\MessageService;

use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\MessageServiceBase;

/**
 * Implementation class of MessageService.
 *
 * You may modify this class for custom logic additions/methode overrides.
 * If you do so, don't forget to call the overriden super-class method!
 *
 * This stub was generated using Spedion\ServiceClientBundle\Command\WSDLInterpreter on Mon, 28 Nov 2011 17:19:08 +0100
 *
 * @see MessageServiceBase
 */
class MessageService extends MessageServiceBase
{

    public function __construct(Credentials $credentials, WSLogger $wsLogger = null, $wsdl = null, $options = array())
    {
        $mergedOptions = array_merge(
            array(
                'login' => $credentials->getUser(),
                'password' => $credentials->getPassword(),
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS 
            ),
            $options
        );
        parent::__construct($wsLogger, $wsdl, $mergedOptions);
    }
    
    public function getLastRequest()
    {
        return $this->getWsHandle()->__getLastRequest();
    }
}