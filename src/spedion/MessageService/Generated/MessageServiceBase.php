<?php
            
namespace Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated;
            
use Spedion\ServiceClientBundle\ServiceClients\MessageService\WSLogger;
            

/********************************************************************************************************/
/* This class was generated using Spedion\ServiceClientBundle\WSDLClientGenerator\WSDLInterpreter\WSDLInterpreter! */
/* Do not modify this file manually!                                                                    */
/* Generated on Thu, 10 Mar 2016 11:41:44 +0100                                                         */
/* WSDL https://simpex.spedion.de/simpex/3.1/Services/MessageService.asmx?WSDL                             */
/********************************************************************************************************/
/**
 * GetMessageCount
 */
class GetMessageCount {
}

/**
 * GetMessageCountResponse
 */
class GetMessageCountResponse {
	/**
	 * @access public
	 * @var integer
	 */
	public $GetMessageCountResult;
}

/**
 * ParseIdentifierString
 */
class ParseIdentifierString {
	/**
	 * @access public
	 * @var string
	 */
	public $vehicleName;
}

/**
 * ParseIdentifierStringResponse
 */
class ParseIdentifierStringResponse {
	/**
	 * @access public
	 * @var boolean
	 */
	public $ParseIdentifierStringResult;
}

/**
 * ParseIdentifierStringSubCustomer
 */
class ParseIdentifierStringSubCustomer {
	/**
	 * @access public
	 * @var string
	 */
	public $vehicleName;
	/**
	 * @access public
	 * @var integer
	 */
	public $subCustomerId;
}

/**
 * ParseIdentifierStringSubCustomerResponse
 */
class ParseIdentifierStringSubCustomerResponse {
	/**
	 * @access public
	 * @var boolean
	 */
	public $ParseIdentifierStringSubCustomerResult;
}

/**
 * GetUnreadMessage
 */
class GetUnreadMessage {
}

/**
 * GetUnreadMessageResponse
 */
class GetUnreadMessageResponse {
	/**
	 * @access public
	 * @var ArrayOfMessage
	 */
	public $GetUnreadMessageResult;
	/**
	 * @access public
	 * @var Result
	 */
	public $r;
}

/**
 * Message
 */
class Message {
	/**
	 * @access public
	 * @var double
	 */
	public $MessageID;
	/**
	 * @access public
	 * @var string
	 */
	public $Vehicle;
	/**
	 * @access public
	 * @var integer
	 */
	public $VehicleId;
	/**
	 * @access public
	 * @var string
	 */
	public $VehicleNumberPlate;
	/**
	 * @access public
	 * @var string
	 */
	public $Text;
	/**
	 * @access public
	 * @var string
	 */
	public $Reference;
	/**
	 * @access public
	 * @var double
	 */
	public $Latitude;
	/**
	 * @access public
	 * @var double
	 */
	public $Longitude;
	/**
	 * @access public
	 * @var Tour
	 */
	public $Tour;
	/**
	 * @access public
	 * @var Place
	 */
	public $Place;
	/**
	 * @access public
	 * @var Order
	 */
	public $Order;
	/**
	 * @access public
	 * @var integer
	 */
	public $Form;
	/**
	 * @access public
	 * @var FmsData
	 */
	public $Fms;
	/**
	 * @access public
	 * @var boolean
	 */
	public $FmsValid;
	/**
	 * @access public
	 * @var AdditionalValues
	 */
	public $AdditionalValues;
	/**
	 * @access public
	 * @var double
	 */
	public $Odometer;
	/**
	 * @access public
	 * @var double
	 */
	public $Speed;
	/**
	 * @access public
	 * @var Address
	 */
	public $LocationInformation;
	/**
	 * @access public
	 * @var User
	 */
	public $Sender;
	/**
	 * @access public
	 * @var string
	 */
	public $FormName;
	/**
	 * @access public
	 * @var Driver
	 */
	public $Driver;
	/**
	 * @access public
	 * @var ArrayOfCoDriver
	 */
	public $CoDrivers;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $Attachment;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $AttachmentFromDispo;
	/**
	 * @access public
	 * @var Gps
	 */
	public $Gps;
	/**
	 * @access public
	 * @var ArrayOfAttachedObject
	 */
	public $AttachedObjects;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $MessageTimeUtc;
	/**
	 * @access public
	 * @var integer
	 */
	public $CustomerId;
}

/**
 * Tour
 */
class Tour {
	/**
	 * @access public
	 * @var string
	 */
	public $Tournr;
	/**
	 * @access public
	 * @var string
	 */
	public $DriverPin;
	/**
	 * @access public
	 * @var integer
	 */
	public $State;
	/**
	 * @access public
	 * @var ArrayOfPlace
	 */
	public $Places;
	/**
	 * @access public
	 * @var AdditionalValues
	 */
	public $AdditionalData;
	/**
	 * @access public
	 * @var string
	 */
	public $RequiredTrailer;
	/**
	 * @access public
	 * @var integer
	 */
	public $DisplayAtPos;
	/**
	 * @access public
	 * @var string
	 */
	public $VehicleName;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $Attachment;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $AttachmentFromDispo;
	/**
	 * @access public
	 * @var string
	 */
	public $Workflow;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentFromDispo;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentToDispo;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TourBeginUtc;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TourEndUtc;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TourDateUtc;
}

/**
 * Place
 */
class Place {
	/**
	 * @access public
	 * @var string
	 */
	public $PlaceNr;
	/**
	 * @access public
	 * @var string
	 */
	public $PlaceName;
	/**
	 * @access public
	 * @var string
	 */
	public $Address;
	/**
	 * @access public
	 * @var string
	 */
	public $Housenr;
	/**
	 * @access public
	 * @var string
	 */
	public $Nation;
	/**
	 * @access public
	 * @var string
	 */
	public $Zip;
	/**
	 * @access public
	 * @var string
	 */
	public $Location;
	/**
	 * @access public
	 * @var ArrayOfOrder
	 */
	public $Orders;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentFromDispo;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentToDispo;
	/**
	 * @access public
	 * @var double
	 */
	public $Latitude;
	/**
	 * @access public
	 * @var double
	 */
	public $Longitude;
	/**
	 * @access public
	 * @var integer
	 */
	public $DisplayAtPos;
	/**
	 * @access public
	 * @var AdditionalValues
	 */
	public $AdditionalData;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $Attachment;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $AttachmentFromDispo;
	/**
	 * @access public
	 * @var integer
	 */
	public $State;
	/**
	 * @access public
	 * @var string
	 */
	public $Workflow;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $PlanBeginUtc;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $PlanEndUtc;
	/**
	 * @access public
	 * @var string
	 */
	public $ExternalRefNr;
}

/**
 * Order
 */
class Order {
	/**
	 * @access public
	 * @var AdditionalValues
	 */
	public $AdditionalData;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentToDispo;
	/**
	 * @access public
	 * @var string
	 */
	public $OrderNr;
	/**
	 * @access public
	 * @var tns_OrderType
	 */
	public $Type;
	/**
	 * @access public
	 * @var string
	 */
	public $ProductName;
	/**
	 * @access public
	 * @var integer
	 */
	public $Amount;
	/**
	 * @access public
	 * @var integer
	 */
	public $Amount2;
	/**
	 * @access public
	 * @var string
	 */
	public $Packing;
	/**
	 * @access public
	 * @var string
	 */
	public $Packing2;
	/**
	 * @access public
	 * @var double
	 */
	public $Weight;
	/**
	 * @access public
	 * @var string
	 */
	public $Hazard;
	/**
	 * @access public
	 * @var string
	 */
	public $Trailer;
	/**
	 * @access public
	 * @var string
	 */
	public $Container1;
	/**
	 * @access public
	 * @var string
	 */
	public $Container2;
	/**
	 * @access public
	 * @var integer
	 */
	public $State;
	/**
	 * @access public
	 * @var string
	 */
	public $OrderNrExtern;
	/**
	 * @access public
	 * @var integer
	 */
	public $DisplayAtPos;
	/**
	 * @access public
	 * @var string
	 */
	public $Name1;
	/**
	 * @access public
	 * @var string
	 */
	public $Name2;
	/**
	 * @access public
	 * @var string
	 */
	public $ArtNr;
	/**
	 * @access public
	 * @var string
	 */
	public $Charge;
	/**
	 * @access public
	 * @var string
	 */
	public $DeliveryNr;
	/**
	 * @access public
	 * @var string
	 */
	public $CommentFromDispo;
	/**
	 * @access public
	 * @var double
	 */
	public $LoadingMeter;
	/**
	 * @access public
	 * @var string
	 */
	public $PackingDescription;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $Attachment;
	/**
	 * @access public
	 * @var Attachment
	 */
	public $AttachmentFromDispo;
	/**
	 * @access public
	 * @var string
	 */
	public $RefNr;
	/**
	 * @access public
	 * @var string
	 */
	public $Workflow;
	/**
	 * @access public
	 * @var string
	 */
	public $ExternalRefNr;
}

/**
 * AdditionalValues
 */
class AdditionalValues {
	/**
	 * @access public
	 * @var ArrayOfAdditionalValueItem
	 */
	public $Items;
}

/**
 * AdditionalValueItem
 */
class AdditionalValueItem {
	/**
	 * @access public
	 * @var string
	 */
	public $Value;
	/**
	 * @access public
	 * @var string
	 */
	public $Key;
}

/**
 * OrderType
 */
class OrderType {
}

/**
 * Attachment
 */
class Attachment {
	/**
	 * @access public
	 * @var ArrayOfUrlBasedAttachment
	 */
	public $UrlBased;
	/**
	 * @access public
	 * @var ArrayOfTextAttachment
	 */
	public $Text;
	/**
	 * @access public
	 * @var ArrayOfBarcodeAttachment
	 */
	public $Barcode;
}

/**
 * UrlBasedAttachment
 */
class UrlBasedAttachment {
	/**
	 * @access public
	 * @var MetaData
	 */
	public $MetaData;
}

/**
 * MetaData
 */
class MetaData {
	/**
	 * @access public
	 * @var string
	 */
	public $WorkflowActionLabel;
	/**
	 * @access public
	 * @var string
	 */
	public $ActionInputResultAttribute;
	/**
	 * @access public
	 * @var string
	 */
        public $Attribute;
	/**
	 * @access public
	 * @var integer
	 */
	public $SortNo;
	/**
	 * @access public
	 * @var integer
	 */
	public $SourceEventForm;
	/**
	 * @access public
	 * @var Position
	 */
	public $Position;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TimestampUtc;
}

/**
 * Position
 */
class Position {
	/**
	 * @access public
	 * @var double
	 */
	public $Latitude;
	/**
	 * @access public
	 * @var double
	 */
	public $Longitude;
	/**
	 * @access public
	 * @var double
	 */
	public $AccuracyInMeter;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TimestampUtc;
}

/**
 * TextAttachment
 */
class TextAttachment {
	/**
	 * @access public
	 * @var MetaData
	 */
	public $MetaData;
	/**
	 * @access public
	 * @var string
	 */
	public $Value;
}

/**
 * BarcodeAttachment
 */
class BarcodeAttachment {
	/**
	 * @access public
	 * @var MetaData
	 */
	public $MetaData;
	/**
	 * @access public
	 * @var ArrayOfString
	 */
	public $Barcode;
}

/**
 * FmsData
 */
class FmsData {
	/**
	 * @access public
	 * @var double
	 */
	public $Speed;
	/**
	 * @access public
	 * @var double
	 */
	public $Totalfuel;
	/**
	 * @access public
	 * @var double
	 */
	public $Odometer;
	/**
	 * @access public
	 * @var double
	 */
	public $Fuellevel;
	/**
	 * @access public
	 * @var double
	 */
	public $NextServiceStop;
	/**
	 * @access public
	 * @var double
	 */
	public $EngineWorkTime;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TimestampUtc;
}

/**
 * Address
 */
class Address {
	/**
	 * @access public
	 * @var string
	 */
	public $CountryCode;
	/**
	 * @access public
	 * @var string
	 */
	public $Place;
	/**
	 * @access public
	 * @var string
	 */
	public $Zip;
	/**
	 * @access public
	 * @var string
	 */
	public $Street;
	/**
	 * @access public
	 * @var string
	 */
	public $HouseNo;
}

/**
 * User
 */
class User {
	/**
	 * @access public
	 * @var string
	 */
	public $Name;
	/**
	 * @access public
	 * @var integer
	 */
	public $Id;
}

/**
 * Driver
 */
class Driver {
	/**
	 * @access public
	 * @var string
	 */
	public $DriverCardId;
	/**
	 * @access public
	 * @var string
	 */
	public $Language;
	/**
	 * @access public
	 * @var string
	 */
	public $LastName;
	/**
	 * @access public
	 * @var string
	 */
	public $FirstName;
	/**
	 * @access public
	 * @var string
	 */
	public $DriverPin;
	/**
	 * @access public
	 * @var integer
	 */
	public $CustomerId;
}

/**
 * CoDriver
 */
class CoDriver extends Driver {
	/**
	 * @access public
	 * @var tns_CoDriverChangeEventEnum
	 */
	public $CoDriverChangeEvent;
}

/**
 * CoDriverChangeEventEnum
 */
class CoDriverChangeEventEnum {
}

/**
 * Gps
 */
class Gps {
	/**
	 * @access public
	 * @var double
	 */
	public $AccuracyInM;
	/**
	 * @access public
	 * @var double
	 */
	public $AltitudeInM;
	/**
	 * @access public
	 * @var boolean
	 */
	public $Fix;
	/**
	 * @access public
	 * @var double
	 */
	public $Direction;
	/**
	 * @access public
	 * @var s_dateTime
	 */
	public $TimestampUtc;
}

/**
 * AttachedObject
 */
class AttachedObject {
	/**
	 * @access public
	 * @var string
	 */
	public $Id;
	/**
	 * @access public
	 * @var string
	 */
	public $Name;
	/**
	 * @access public
	 * @var string
	 */
	public $Type;
	/**
	 * @access public
	 * @var string
	 */
	public $Change;
	/**
	 * @access public
	 * @var string
	 */
        public $SubCategory;
	/**
	 * @access public
	 * @var string
	 */
        public $AttachedToId;
	/**
	 * @access public
	 * @var string
	 */
        public $AttachedToName;
}

/**
 * Result
 */
class Result {
	/**
	 * @access public
	 * @var tns_ResultCode
	 */
	public $Code;
	/**
	 * @access public
	 * @var string
	 */
	public $Description;
	/**
	 * @access public
	 * @var integer
	 */
	public $ErrorCode;
	/**
	 * @access public
	 * @var string
	 */
	public $ErrorDesc;
}

/**
 * ResultCode
 */
class ResultCode {
}

/**
 * GetNewMessage
 */
class GetNewMessage {
}

/**
 * GetNewMessageResponse
 */
class GetNewMessageResponse {
	/**
	 * @access public
	 * @var ArrayOfMessage
	 */
	public $GetNewMessageResult;
}

/**
 * GetMessageId
 */
class GetMessageId {
	/**
	 * @access public
	 * @var double
	 */
	public $messageId;
}

/**
 * GetMessageIdResponse
 */
class GetMessageIdResponse {
	/**
	 * @access public
	 * @var Message
	 */
	public $GetMessageIdResult;
}

/**
 * AddMessage
 */
class AddMessage {
	/**
	 * @access public
	 * @var Message
	 */
	public $message;
}

/**
 * AddMessageResponse
 */
class AddMessageResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $AddMessageResult;
}

/**
 * GetPresignedUploadUrl
 */
class GetPresignedUploadUrl {
	/**
	 * @access public
	 * @var string
	 */
	public $filename;
	/**
	 * @access public
	 * @var integer
	 */
	public $filesize;
}

/**
 * GetPresignedUploadUrlResponse
 */
class GetPresignedUploadUrlResponse {
	/**
	 * @access public
	 * @var PresignedUploadUrlResult
	 */
	public $GetPresignedUploadUrlResult;
}

/**
 * PresignedUploadUrlResult
 */
class PresignedUploadUrlResult {
	/**
	 * @access public
	 * @var tns_ResultCode
	 */
	public $ResultCode;
	/**
	 * @access public
	 * @var tns_ErrorCode
	 */
	public $ErrorCode;
	/**
	 * @access public
	 * @var string
	 */
	public $AttachmentUrl;
	/**
	 * @access public
	 * @var string
	 */
	public $UploadUrl;
	/**
	 * @access public
	 * @var string
	 */
	public $Filename;
}

/**
 * ErrorCode
 */
class ErrorCode {
}

/**
 * GetToursByVehicle
 */
class GetToursByVehicle {
	/**
	 * @access public
	 * @var string
	 */
	public $vehicleName;
	/**
	 * @access public
	 * @var integer
	 */
	public $skipCount;
	/**
	 * @access public
	 * @var integer
	 */
	public $takeCount;
}

/**
 * GetToursByVehicleResponse
 */
class GetToursByVehicleResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByVehicleResult;
}

/**
 * GetToursByVehicleAndState
 */
class GetToursByVehicleAndState {
	/**
	 * @access public
	 * @var string
	 */
	public $vehicleName;
	/**
	 * @access public
	 * @var integer
	 */
	public $skipCount;
	/**
	 * @access public
	 * @var integer
	 */
	public $takeCount;
	/**
	 * @access public
	 * @var integer
	 */
	public $minState;
	/**
	 * @access public
	 * @var integer
	 */
	public $maxState;
}

/**
 * GetToursByVehicleAndStateResponse
 */
class GetToursByVehicleAndStateResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByVehicleAndStateResult;
}

/**
 * GetToursBySubCustomerVehicle
 */
class GetToursBySubCustomerVehicle {
	/**
	 * @access public
	 * @var integer
	 */
	public $subCustomerId;
	/**
	 * @access public
	 * @var string
	 */
	public $vehicleName;
	/**
	 * @access public
	 * @var integer
	 */
	public $skipCount;
	/**
	 * @access public
	 * @var integer
	 */
	public $takeCount;
}

/**
 * GetToursBySubCustomerVehicleResponse
 */
class GetToursBySubCustomerVehicleResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursBySubCustomerVehicleResult;
}

/**
 * AddRegistrationMessage
 */
class AddRegistrationMessage {
	/**
	 * @access public
	 * @var RegistrationMessage
	 */
	public $m;
}

/**
 * RegistrationMessage
 */
class RegistrationMessage {
	/**
	 * @access public
	 * @var string
	 */
	public $ReferenceNr;
	/**
	 * @access public
	 * @var string
	 */
	public $Vehicle;
	/**
	 * @access public
	 * @var Driver
	 */
	public $Driver;
	/**
	 * @access public
	 * @var string
	 */
	public $DriverMobilePhoneNr;
	/**
	 * @access public
	 * @var string
	 */
	public $DriverEmailAdress;
	/**
	 * @access public
	 * @var string
	 */
	public $Comment;
	/**
	 * @access public
	 * @var string
	 */
	public $DispoPhoneNr;
	/**
	 * @access public
	 * @var integer
	 */
	public $CustomerId;
	/**
	 * @access public
	 * @var boolean
	 */
	public $UseDummyDriverIfDriverIsNull;
	/**
	 * @access public
	 * @var string
	 */
	public $LanguageCode;
	/**
	 * @access public
	 * @var integer
	 */
	public $UserExpiresTimespanInSeconds;
	/**
	 * @access public
	 * @var tns_RegistrationMessageTextType
	 */
	public $TextStyle;
}

/**
 * RegistrationMessageTextType
 */
class RegistrationMessageTextType {
}

/**
 * AddRegistrationMessageResponse
 */
class AddRegistrationMessageResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $AddRegistrationMessageResult;
}

/**
 * GetToursByExample
 */
class GetToursByExample {
	/**
	 * @access public
	 * @var Tour
	 */
	public $example;
}

/**
 * GetToursByExampleResponse
 */
class GetToursByExampleResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByExampleResult;
}

/**
 * GetTourByTourNr
 */
class GetTourByTourNr {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
}

/**
 * GetTourByTourNrResponse
 */
class GetTourByTourNrResponse {
	/**
	 * @access public
	 * @var Tour
	 */
	public $GetTourByTourNrResult;
}

/**
 * GetCustomerTourForAllSubCustomersByTourNr
 */
class GetCustomerTourForAllSubCustomersByTourNr {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
}

/**
 * GetCustomerTourForAllSubCustomersByTourNrResponse
 */
class GetCustomerTourForAllSubCustomersByTourNrResponse {
	/**
	 * @access public
	 * @var CustomerTour
	 */
	public $GetCustomerTourForAllSubCustomersByTourNrResult;
}

/**
 * CustomerTour
 */
class CustomerTour {
	/**
	 * @access public
	 * @var integer
	 */
	public $CustomerId;
	/**
	 * @access public
	 * @var Tour
	 */
	public $Tour;
}

/**
 * GetTourByTourNrOfCustomer
 */
class GetTourByTourNrOfCustomer {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
	/**
	 * @access public
	 * @var integer
	 */
	public $customer;
}

/**
 * GetTourByTourNrOfCustomerResponse
 */
class GetTourByTourNrOfCustomerResponse {
	/**
	 * @access public
	 * @var Tour
	 */
	public $GetTourByTourNrOfCustomerResult;
}

/**
 * GetToursByPlaceNrs
 */
class GetToursByPlaceNrs {
	/**
	 * @access public
	 * @var ArrayOfString
	 */
	public $placeNrs;
}

/**
 * GetToursByPlaceNrsResponse
 */
class GetToursByPlaceNrsResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByPlaceNrsResult;
}

/**
 * GetToursByOrderNrs
 */
class GetToursByOrderNrs {
	/**
	 * @access public
	 * @var ArrayOfString
	 */
	public $orderNrs;
}

/**
 * GetToursByOrderNrsResponse
 */
class GetToursByOrderNrsResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByOrderNrsResult;
}

/**
 * GetToursByOrderNrExtern
 */
class GetToursByOrderNrExtern {
	/**
	 * @access public
	 * @var string
	 */
	public $orderNrExtern;
}

/**
 * GetToursByOrderNrExternResponse
 */
class GetToursByOrderNrExternResponse {
	/**
	 * @access public
	 * @var ArrayOfTour
	 */
	public $GetToursByOrderNrExternResult;
}

/**
 * DeleteTour
 */
class DeleteTour {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
}

/**
 * DeleteTourResponse
 */
class DeleteTourResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $DeleteTourResult;
}

/**
 * DeleteSubCustomerTour
 */
class DeleteSubCustomerTour {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
	/**
	 * @access public
	 * @var integer
	 */
	public $subCustomerId;
}

/**
 * DeleteSubCustomerTourResponse
 */
class DeleteSubCustomerTourResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $DeleteSubCustomerTourResult;
}

/**
 * AbandonTourByTourNr
 */
class AbandonTourByTourNr {
	/**
	 * @access public
	 * @var string
	 */
	public $TourNr;
}

/**
 * AbandonTourByTourNrResponse
 */
class AbandonTourByTourNrResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $AbandonTourByTourNrResult;
}

/**
 * DeleteTourObject
 */
class DeleteTourObject {
	/**
	 * @access public
	 * @var Tour
	 */
	public $wsTour;
}

/**
 * DeleteTourObjectResponse
 */
class DeleteTourObjectResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $DeleteTourObjectResult;
}

/**
 * UpdateTour
 */
class UpdateTour {
	/**
	 * @access public
	 * @var Tour
	 */
	public $newTourdata;
}

/**
 * UpdateTourResponse
 */
class UpdateTourResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $UpdateTourResult;
}

/**
 * UpdateSubCustomerTour
 */
class UpdateSubCustomerTour {
	/**
	 * @access public
	 * @var Tour
	 */
	public $newTourdata;
	/**
	 * @access public
	 * @var integer
	 */
	public $subCustomerId;
}

/**
 * UpdateSubCustomerTourResponse
 */
class UpdateSubCustomerTourResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $UpdateSubCustomerTourResult;
}

/**
 * ReportError
 */
class ReportError {
	/**
	 * @access public
	 * @var string
	 */
	public $ErrorReport;
}

/**
 * ReportErrorResponse
 */
class ReportErrorResponse {
}

/**
 * SetListOfMessageAsRead
 */
class SetListOfMessageAsRead {
	/**
	 * @access public
	 * @var ArrayOfDecimal
	 */
	public $messageIdList;
}

/**
 * SetListOfMessageAsReadResponse
 */
class SetListOfMessageAsReadResponse {
	/**
	 * @access public
	 * @var Result
	 */
	public $SetListOfMessageAsReadResult;
}

/**
 * GetVersion
 */
class GetVersion {
}

/**
 * GetVersionResponse
 */
class GetVersionResponse {
	/**
	 * @access public
	 * @var string
	 */
	public $GetVersionResult;
}
            
/**
 * MessageService
 * @author Spedion\ServiceClientBundle\WSDLClientGenerator\WSDLInterpreter\WSDLInterpreter
 */
abstract class MessageServiceBase {
	/**
	 * Ws handle
	 * @access private
	 * @var \SoapClient
	 */
	private $wsHandle;

	private $wsLogger;

	/**
	 * Constructor using WSLogger, wsdl location and options array
	 * 'features' => 1 = Konstante SOAP_SINGLE_ELEMENT_ARRAYS, erzeugt durch WSDL Generator
	 * @param WSLogger $wsLogger A WSLogger instance for profiling, or null
	 * @param string $wsdl WSDL location for this service
	 * @param array $options Options for the SoapClient
	 */
	public function __construct(WSLogger $wsLogger = null, $wsdl=null, $options=array()) {
		if(!$wsdl) $wsdl = "https://simpex.spedion.de/simpex/3.1/Services/MessageService.asmx?WSDL";
		$this->wsLogger = $wsLogger;
                $mergedOptions = array_merge(
                    array (
                        'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                    ),
                    $options
                );
		$this->wsHandle = new \SoapClient($wsdl, $mergedOptions);
	}

	/**
	 * Service Call: GetMessageCount
	 * @param GetMessageCount $GetMessageCount
	 * @return GetMessageCountResponse|int
	 * @throws \Exception invalid function signature message
	 */
	public function GetMessageCount(GetMessageCount $GetMessageCount) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetMessageCount));
		$wsResult = $this->wsHandle->__soapCall("GetMessageCount", array($GetMessageCount));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: ParseIdentifierString
	 * @param ParseIdentifierString $ParseIdentifierString
	 * @return ParseIdentifierStringResponse|boolean
	 * @throws \Exception invalid function signature message
	 */
	public function ParseIdentifierString(ParseIdentifierString $ParseIdentifierString) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($ParseIdentifierString));
		$wsResult = $this->wsHandle->__soapCall("ParseIdentifierString", array($ParseIdentifierString));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: ParseIdentifierStringSubCustomer
	 * @param ParseIdentifierStringSubCustomer $ParseIdentifierStringSubCustomer
	 * @return ParseIdentifierStringSubCustomerResponse|boolean
	 * @throws \Exception invalid function signature message
	 */
	public function ParseIdentifierStringSubCustomer(ParseIdentifierStringSubCustomer $ParseIdentifierStringSubCustomer) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($ParseIdentifierStringSubCustomer));
		$wsResult = $this->wsHandle->__soapCall("ParseIdentifierStringSubCustomer", array($ParseIdentifierStringSubCustomer));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetUnreadMessage
	 * @param GetUnreadMessage $GetUnreadMessage
	 * @return GetUnreadMessageResponse
	 * @throws \Exception invalid function signature message
	 */
	public function GetUnreadMessage(GetUnreadMessage $GetUnreadMessage) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetUnreadMessage));
		$wsResult = $this->wsHandle->__soapCall("GetUnreadMessage", array($GetUnreadMessage));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetNewMessage
	 * @param GetNewMessage $GetNewMessage
	 * @return GetNewMessageResponse|ArrayOfMessage
	 * @throws \Exception invalid function signature message
	 */
	public function GetNewMessage(GetNewMessage $GetNewMessage) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetNewMessage));
		$wsResult = $this->wsHandle->__soapCall("GetNewMessage", array($GetNewMessage));
		if(isset($wsResult->GetNewMessageResult->Message)) {
			if(!is_array($wsResult->GetNewMessageResult->Message)) {
				$wsResult->GetNewMessageResult->Message = array($wsResult->GetNewMessageResult->Message);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetMessageId
	 * @param GetMessageId $GetMessageId
	 * @return GetMessageIdResponse|Message
	 * @throws \Exception invalid function signature message
	 */
	public function GetMessageId(GetMessageId $GetMessageId) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetMessageId));
		$wsResult = $this->wsHandle->__soapCall("GetMessageId", array($GetMessageId));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: AddMessage
	 * @param AddMessage $AddMessage
	 * @return AddMessageResponse
	 * @throws \Exception invalid function signature message
	 */
	public function AddMessage(AddMessage $AddMessage) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($AddMessage));
		$wsResult = $this->wsHandle->__soapCall("AddMessage", array($AddMessage));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetPresignedUploadUrl
	 * @param GetPresignedUploadUrl $GetPresignedUploadUrl
	 * @return GetPresignedUploadUrlResponse|PresignedUploadUrlResult
	 * @throws \Exception invalid function signature message
	 */
	public function GetPresignedUploadUrl(GetPresignedUploadUrl $GetPresignedUploadUrl) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetPresignedUploadUrl));
		$wsResult = $this->wsHandle->__soapCall("GetPresignedUploadUrl", array($GetPresignedUploadUrl));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByVehicle
	 * @param GetToursByVehicle $GetToursByVehicle
	 * @return GetToursByVehicleResponse|ArrayOfTour
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByVehicle(GetToursByVehicle $GetToursByVehicle) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByVehicle));
		$wsResult = $this->wsHandle->__soapCall("GetToursByVehicle", array($GetToursByVehicle));
		if(isset($wsResult->GetToursByVehicleResult->Tour)) {
			if(!is_array($wsResult->GetToursByVehicleResult->Tour)) {
				$wsResult->GetToursByVehicleResult->Tour = array($wsResult->GetToursByVehicleResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByVehicleAndState
	 * @param GetToursByVehicleAndState $GetToursByVehicleAndState
	 * @return GetToursByVehicleAndStateResponse|ArrayOfTour
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByVehicleAndState(GetToursByVehicleAndState $GetToursByVehicleAndState) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByVehicleAndState));
		$wsResult = $this->wsHandle->__soapCall("GetToursByVehicleAndState", array($GetToursByVehicleAndState));
		if(isset($wsResult->GetToursByVehicleAndStateResult->Tour)) {
			if(!is_array($wsResult->GetToursByVehicleAndStateResult->Tour)) {
				$wsResult->GetToursByVehicleAndStateResult->Tour = array($wsResult->GetToursByVehicleAndStateResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursBySubCustomerVehicle
	 * @param GetToursBySubCustomerVehicle $GetToursBySubCustomerVehicle
	 * @return GetToursBySubCustomerVehicleResponse|ArrayOfTour
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursBySubCustomerVehicle(GetToursBySubCustomerVehicle $GetToursBySubCustomerVehicle) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursBySubCustomerVehicle));
		$wsResult = $this->wsHandle->__soapCall("GetToursBySubCustomerVehicle", array($GetToursBySubCustomerVehicle));
		if(isset($wsResult->GetToursBySubCustomerVehicleResult->Tour)) {
			if(!is_array($wsResult->GetToursBySubCustomerVehicleResult->Tour)) {
				$wsResult->GetToursBySubCustomerVehicleResult->Tour = array($wsResult->GetToursBySubCustomerVehicleResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: AddRegistrationMessage
	 * @param AddRegistrationMessage $AddRegistrationMessage
	 * @return AddRegistrationMessageResponse
	 * @throws \Exception invalid function signature message
	 */
	public function AddRegistrationMessage(AddRegistrationMessage $AddRegistrationMessage) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($AddRegistrationMessage));
		$wsResult = $this->wsHandle->__soapCall("AddRegistrationMessage", array($AddRegistrationMessage));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByExample
	 * @param GetToursByExample $GetToursByExample
	 * @return GetToursByExampleResponse
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByExample(GetToursByExample $GetToursByExample) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByExample));
		$wsResult = $this->wsHandle->__soapCall("GetToursByExample", array($GetToursByExample));
		if(isset($wsResult->GetToursByExampleResult->Tour)) {
			if(!is_array($wsResult->GetToursByExampleResult->Tour)) {
				$wsResult->GetToursByExampleResult->Tour = array($wsResult->GetToursByExampleResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetTourByTourNr
	 * @param GetTourByTourNr $GetTourByTourNr
	 * @return GetTourByTourNrResponse|Tour
	 * @throws \Exception invalid function signature message
	 */
	public function GetTourByTourNr(GetTourByTourNr $GetTourByTourNr) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetTourByTourNr));
		$wsResult = $this->wsHandle->__soapCall("GetTourByTourNr", array($GetTourByTourNr));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetCustomerTourForAllSubCustomersByTourNr
	 * @param GetCustomerTourForAllSubCustomersByTourNr $GetCustomerTourForAllSubCustomersByTourNr
	 * @return GetCustomerTourForAllSubCustomersByTourNrResponse|CustomerTour
	 * @throws \Exception invalid function signature message
	 */
	public function GetCustomerTourForAllSubCustomersByTourNr(GetCustomerTourForAllSubCustomersByTourNr $GetCustomerTourForAllSubCustomersByTourNr) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetCustomerTourForAllSubCustomersByTourNr));
		$wsResult = $this->wsHandle->__soapCall("GetCustomerTourForAllSubCustomersByTourNr", array($GetCustomerTourForAllSubCustomersByTourNr));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetTourByTourNrOfCustomer
	 * @param GetTourByTourNrOfCustomer $GetTourByTourNrOfCustomer
	 * @return GetTourByTourNrOfCustomerResponse|Tour
	 * @throws \Exception invalid function signature message
	 */
	public function GetTourByTourNrOfCustomer(GetTourByTourNrOfCustomer $GetTourByTourNrOfCustomer) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetTourByTourNrOfCustomer));
		$wsResult = $this->wsHandle->__soapCall("GetTourByTourNrOfCustomer", array($GetTourByTourNrOfCustomer));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByPlaceNrs
	 * @param GetToursByPlaceNrs $GetToursByPlaceNrs
	 * @return GetToursByPlaceNrsResponse
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByPlaceNrs(GetToursByPlaceNrs $GetToursByPlaceNrs) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByPlaceNrs));
		$wsResult = $this->wsHandle->__soapCall("GetToursByPlaceNrs", array($GetToursByPlaceNrs));
		if(isset($wsResult->GetToursByPlaceNrsResult->Tour)) {
			if(!is_array($wsResult->GetToursByPlaceNrsResult->Tour)) {
				$wsResult->GetToursByPlaceNrsResult->Tour = array($wsResult->GetToursByPlaceNrsResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByOrderNrs
	 * @param GetToursByOrderNrs $GetToursByOrderNrs
	 * @return GetToursByOrderNrsResponse
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByOrderNrs(GetToursByOrderNrs $GetToursByOrderNrs) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByOrderNrs));
		$wsResult = $this->wsHandle->__soapCall("GetToursByOrderNrs", array($GetToursByOrderNrs));
		if(isset($wsResult->GetToursByOrderNrsResult->Tour)) {
			if(!is_array($wsResult->GetToursByOrderNrsResult->Tour)) {
				$wsResult->GetToursByOrderNrsResult->Tour = array($wsResult->GetToursByOrderNrsResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetToursByOrderNrExtern
	 * @param GetToursByOrderNrExtern $GetToursByOrderNrExtern
	 * @return GetToursByOrderNrExternResponse|ArrayOfTour
	 * @throws \Exception invalid function signature message
	 */
	public function GetToursByOrderNrExtern(GetToursByOrderNrExtern $GetToursByOrderNrExtern) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetToursByOrderNrExtern));
		$wsResult = $this->wsHandle->__soapCall("GetToursByOrderNrExtern", array($GetToursByOrderNrExtern));
		if(isset($wsResult->GetToursByOrderNrExternResult->Tour)) {
			if(!is_array($wsResult->GetToursByOrderNrExternResult->Tour)) {
				$wsResult->GetToursByOrderNrExternResult->Tour = array($wsResult->GetToursByOrderNrExternResult->Tour);
			}
		}
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: DeleteTour
	 * @param DeleteTour $DeleteTour
	 * @return DeleteTourResponse|Result
	 * @throws \Exception invalid function signature message
	 */
	public function DeleteTour(DeleteTour $DeleteTour) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($DeleteTour));
		$wsResult = $this->wsHandle->__soapCall("DeleteTour", array($DeleteTour));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: DeleteSubCustomerTour
	 * @param DeleteSubCustomerTour $DeleteSubCustomerTour
	 * @return DeleteSubCustomerTourResponse|Result
	 * @throws \Exception invalid function signature message
	 */
	public function DeleteSubCustomerTour(DeleteSubCustomerTour $DeleteSubCustomerTour) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($DeleteSubCustomerTour));
		$wsResult = $this->wsHandle->__soapCall("DeleteSubCustomerTour", array($DeleteSubCustomerTour));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: AbandonTourByTourNr
	 * @param AbandonTourByTourNr $AbandonTourByTourNr
	 * @return AbandonTourByTourNrResponse|Result
	 * @throws \Exception invalid function signature message
	 */
	public function AbandonTourByTourNr(AbandonTourByTourNr $AbandonTourByTourNr) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($AbandonTourByTourNr));
		$wsResult = $this->wsHandle->__soapCall("AbandonTourByTourNr", array($AbandonTourByTourNr));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: DeleteTourObject
	 * @param DeleteTourObject $DeleteTourObject
	 * @return DeleteTourObjectResponse
	 * @throws \Exception invalid function signature message
	 */
	public function DeleteTourObject(DeleteTourObject $DeleteTourObject) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($DeleteTourObject));
		$wsResult = $this->wsHandle->__soapCall("DeleteTourObject", array($DeleteTourObject));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: UpdateTour
	 * @param UpdateTour $UpdateTour
	 * @return UpdateTourResponse
	 * @throws \Exception invalid function signature message
	 */
	public function UpdateTour(UpdateTour $UpdateTour) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($UpdateTour));
		$wsResult = $this->wsHandle->__soapCall("UpdateTour", array($UpdateTour));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: UpdateSubCustomerTour
	 * @param UpdateSubCustomerTour $UpdateSubCustomerTour
	 * @return UpdateSubCustomerTourResponse
	 * @throws \Exception invalid function signature message
	 */
	public function UpdateSubCustomerTour(UpdateSubCustomerTour $UpdateSubCustomerTour) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($UpdateSubCustomerTour));
		$wsResult = $this->wsHandle->__soapCall("UpdateSubCustomerTour", array($UpdateSubCustomerTour));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: ReportError
	 * @param ReportError $ReportError
	 * @return ReportErrorResponse
	 * @throws \Exception invalid function signature message
	 */
	public function ReportError(ReportError $ReportError) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($ReportError));
		$wsResult = $this->wsHandle->__soapCall("ReportError", array($ReportError));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: SetListOfMessageAsRead
	 * @param SetListOfMessageAsRead $SetListOfMessageAsRead
	 * @return SetListOfMessageAsReadResponse
	 * @throws \Exception invalid function signature message
	 */
	public function SetListOfMessageAsRead(SetListOfMessageAsRead $SetListOfMessageAsRead) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($SetListOfMessageAsRead));
		$wsResult = $this->wsHandle->__soapCall("SetListOfMessageAsRead", array($SetListOfMessageAsRead));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	/**
	 * Service Call: GetVersion
	 * @param GetVersion $GetVersion
	 * @return GetVersionResponse|string
	 * @throws \Exception invalid function signature message
	 */
	public function GetVersion(GetVersion $GetVersion) {
		if($this->wsLogger) $this->wsLogger->startCall(__CLASS__, __METHOD__, array($GetVersion));
		$wsResult = $this->wsHandle->__soapCall("GetVersion", array($GetVersion));
		if($this->wsLogger) $this->wsLogger->stopCall();
		return $wsResult;
	}


	protected function getWsHandle() {
		return $this->wsHandle;
	}
}