<?php

namespace Spedion;

use Spedion\ServiceClientBundle\ServiceClients\MessageService\MessageService;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\GetVersion;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\WSLogger;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Tour;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\GetTourByTourNr;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\UpdateTour;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Credentials;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\AddMessage;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\Message;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\GetMessageId;
use Spedion\ServiceClientBundle\ServiceClients\MessageService\Generated\GetUnreadMessage;

/**
 * Description of MessageServiceProxy
 *
 * @author jbayer
 */
class MessageServiceFacade
{

    /**
     * @var MessageService
     */
    private $messageService;

    public function __construct(Credentials $credentials, $wsdl = null, array $options = array())
    {
        $mergedOptions = array_merge(
            array(
                'trace' => 1,
                'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                'cache_wsdl' => WSDL_CACHE_BOTH
            ),
            $options
        );
        $this->messageService = new MessageService(
                $credentials,
                new WSLogger(),
                $wsdl,
                $mergedOptions
            );
    }

    public function GetVersion()
    {
        try {
            $sendtextParam = new GetVersion();
            $version = $this->messageService->GetVersion($sendtextParam);
            return $version->GetVersionResult;
        } catch (\Exception $e) {
            $this->handleException($e);
        }
        if (strlen($version->GetVersionResult) < 7) {
            throw new \Exception('Could not connect to Simpex MessageService!');
        }
        return null;
    }

    /**
     * 
     * @param Message $message
     * @throws \Exception
     * @return mixed Description
     */
    public function AddMessage(Message $message)
    {
        if ($message == null) {
            throw new \Exception('The message property must not be null!');
        }
        $result = null;
        if (isset($message->Tour) && $message->Tour instanceof Tour && $this->TourExists($message->Tour)) {
            $updateTour = new UpdateTour();
            $updateTour->newTourdata = $message->Tour;
            $result = $this->messageService->UpdateTour($updateTour);
        } else {
            try {
                // needs an extra encapsulation, to match wsdl serialization
                $addMessage = new AddMessage();
                $addMessage->message = $message;
                $result = $this->messageService->AddMessage($addMessage);
            } catch (\Exception $ex) {
                $this->handleException($ex);
            }
        }
        return $result;
    }
    
    public function GetMessageById($messageId)
    {
        if ($messageId < 1) {
            throw new \Exception('No message id given.');
        }
        $getMessageId = new GetMessageId();
        $getMessageId->messageId = $messageId;
        $result = null;
        try {
            $result = $this->messageService->GetMessageId($getMessageId);
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return $result;
    }
    
    /**
     * @return Facade\GetUnreadMessageResponse
     */
    public function GetUnreadMessage()
    {
        try {
            $messages = $this->messageService->GetUnreadMessage(new GetUnreadMessage());
            $result = new Facade\GetUnreadMessageResponse();
            if (isset($messages->GetUnreadMessageResult) && isset($messages->GetUnreadMessageResult->Message)) {
                $result->messages = $messages->GetUnreadMessageResult->Message;
            }
            if (isset($messages->r)) {
                $result->result = $messages->r;
            }
            return $result;
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return null;
    }
    
    /**
     * 
     * @param array $messageIdList
     * @return Result
     */
    public function SetListOfMessageAsRead(array $messageIdList)
    {
        $setListOfMessageAsRead = new ServiceClientBundle\ServiceClients\MessageService\Generated\SetListOfMessageAsRead();
        $setListOfMessageAsRead->messageIdList = $messageIdList;
        try {
            $response = $this->messageService->SetListOfMessageAsRead($setListOfMessageAsRead);
            return $response->SetListOfMessageAsReadResult;
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return null;
    }
    
    /**
     * 
     * @param Tour $tour
     * @return Result
     */
    public function UpdateTour(Tour $tour)
    {
        $updateTour = new UpdateTour();
        $updateTour->newTourdata = $tour;
        try {
            $res = $this->messageService->UpdateTour($updateTour);
            if ($res != null && isset($res->UpdateTourResult)) {
                return $res->UpdateTourResult;
            }
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return null;
    }
    
    public function DeleteTourByTournr($tournr)
    {
        $deleteTour = new ServiceClientBundle\ServiceClients\MessageService\Generated\DeleteTour();
        $deleteTour->TourNr = $tournr;
        try {
            $res = $this->messageService->DeleteTour($deleteTour);
            if ($res != null && isset($res->DeleteTourResult)) {
                return $res->DeleteTourResult;
            }
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return null;
    }
    
    /**
     * 
     * @param string $tournr
     * @return Tour Simpex Tour
     */
    public function GetTourByTourNr($tournr)
    {
        $param = new GetTourByTourNr();
        $param->TourNr = $tournr;
        try {
            $result = $this->messageService->GetTourByTourNr($param);
            if (isset($result->GetTourByTourNrResult) && $result->GetTourByTourNrResult !== null) {
                return $result->GetTourByTourNrResult;
            }
        } catch (\Exception $ex) {
            $this->handleException($ex);
        }
        return null;
    }

    /**
     * 
     * @param Tour $tour
     * @return boolean
     */
    public function TourExists(Tour $tour)
    {
        $res = $this->GetTourByTourNr($tour->Tournr);
        return $res != null && $res instanceof Tour;
    }
    
    private function handleException(\Exception $ex)
    {
        echo 'MessageServiceFacade.handleException():' . PHP_EOL;
        echo $ex->getMessage();
        echo PHP_EOL . PHP_EOL;
        echo $this->messageService->getLastRequest();
    }
    
    public function GetLastRequest()
    {
        return $this->messageService->getLastRequest();
    }

}