<?php

namespace Spedion;

/**
 * Some helper functions
 *
 * @author jbayer
 */
abstract class Simpex
{
    public static function dateTimeFormat(\DateTime $dateTime)
    {
        $dateTime->setTimezone(new \DateTimeZone('UTC'));
        return $dateTime->format('Y-m-d\TH:i:s\Z');
    }
}
